import React from "react";

import MessageView from "../message-view";

class MessagesList extends React.Component {
    state = {
        messages:  [
            {
                from: 'John',
                content: 'The event will start next week',
                status: 'unread'
            },
            {
                from: 'Martha',
                content: 'I will be traveling soon',
                status: 'read'
            },
            {
                from: 'Jacob',
                content: 'Talk later. Have a great day!',
                status: 'read'
            }
        ]
    }
    message;

    render() {
        const messageViews = this.state.messages.map((message, index) =>
                <MessageView key={index} message={message} />
        )

        return(
            <div>
                <h1>List of Messages</h1>
                { messageViews }
            </div>
        )
    }
}

export default MessagesList;
