import React from 'react';
import './App.css';

import MessagesList from "./components/messages/list/messages-list";

class App extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <MessagesList />
    );
  }
}

export default App;
