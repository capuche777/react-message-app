import React from "react";

import './message-view.css'

class MessageView extends React.Component {
    render() {
        return(
            <div className="container">
                <div className="from">
                    <span className="label">From: </span>
                    <span className="value">{ this.props.message.from }</span>
                </div>
                <div className="status">
                    <span className="label">Status: </span>
                    <span className="value">{ this.props.message.status }</span>
                </div>
                <div className="message">
                    <span className="label">Message: </span>
                    <span className="value">{ this.props.message.content }</span>
                </div>
            </div>
        )
    }
}

export default MessageView;
